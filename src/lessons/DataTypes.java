package lessons;

public class DataTypes {
    public static void main(String[] args) {
        byte b = 10;
        byte c = (byte)(b + 2);
        int a = b;

        float f = 10.2F;

        double d = 12.5d;

        char v = '\u00C6';
        char v1 = 198;

        System.out.println(v1);

    }
}
