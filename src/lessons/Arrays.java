package lessons;

public class Arrays {
    public static void main(String[] args) {
//        datatype[] name_array = new datatype[size];
        int[] array = new int[10];
        int[] array1 = new int[]{1, 2, 3};
        int[] array2 = {1, 2, 3};

        for (int i = 0; i < 10; ++i) {
            array[i] = i + 1;
        }

        int a = 20;

        while (a < 10) {
            array[a] = a + 2;
            ++a;
        }

        int d = 0;

        do {

        } while (d < 10);

        for (int e : array) {
            System.out.println(e);
        }


    }
}
