package homeworks.imitation_list;

import java.util.Random;
import java.util.Scanner;

public class ImitationArrayList {
    private int arrayLength = 0;//remove
    private int arrayAuxiliaryLength = 0;
    private int counter = 0;//remove
    private int[] arr;
    private int[] arrAuxiliary;

    void initArray() {                                      // Initializiion of Array
        System.out.println("Enter Array size");
        Scanner arrLength = new Scanner(System.in);//move to menu
        arrayLength = arrLength.nextInt();
        Random arrDeclaration = new Random();
        arr = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            arr[i] = arrDeclaration.nextInt(15);
        }
        System.out.println("Ok. Your array is:");
        for (int z : arr) {//duplication
            System.out.print(z + " ");
        }
    }

    void arrAddElement(int valueOfElement) {//change name                // Add an element to Array
        arrAuxiliary = new int[++arrayLength];
        for (int i = 0; i < arrayLength - 1; i++) {
            arrAuxiliary[i] = arr[i];
        }
        arrAuxiliary[--arrayLength] = valueOfElement;
        System.out.println("\n" + "You added a new element and now your Array modified and looks like:");

        for (int i : arrAuxiliary) {
            System.out.print(i + " ");
        }

    }

    void arrChangeElement(int numberOfElement, int valueOfElement) {            // Change an elemenent of Array by index number
        int[] arrChangeElement = new int[arrayLength];

        for (int y = 0; y < arrChangeElement.length; y++) {//remove
            arrChangeElement[y] = arr[y];
        }

        arrChangeElement[numberOfElement] = valueOfElement;
        System.out.println("\n" + "You changed element # " + numberOfElement + " and now your Array modified and looks like:");
        for (int i : arrChangeElement) {
            System.out.print(i + " ");
        }

    }

    void removeElementByIndex(int index) {                       // Remove an element of Array by index number
        int[] arrAuxiliaryRemove = new int[--arrayLength];
        for (int i = 0; i < index; i++) {
            arrAuxiliaryRemove[i] = arr[i];
        }
       /* for (int y = index, c = ++index; y < arrAuxiliaryRemove.length; y++, c++) {
            arrAuxiliaryRemove[y] = arr[c];
        } */

        for (int y = index; y < arrAuxiliaryRemove.length; y++) {
            arrAuxiliaryRemove[y] = arr[y + 1];
        }
        System.out.println("\n" + "You removed element # " + index + " and now your Array modified and looks like:");
        for (int z : arrAuxiliaryRemove) {
            System.out.print(z + " ");
        }
    }

    void arrChangeLength(int arrLength) {                   // Change a length of Array
        int[] arrAuxiliaryLength = new int[arrLength];
        for (int i = 0; i < arrLength; i++) {
            arrAuxiliaryLength[i] = arr[i];
        }
        System.out.println("\n" + "You changed length of an Array and now it's looks like:");
        for (int y : arrAuxiliaryLength) {
            System.out.print(y + " ");
        }
    }

    void arrOutput() {                    // Outputs of Array (direct and reverse)
        System.out.println("\n" + "Array's direct output:");
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println("\n" + "Array's reverse output:");
        for (int y = arr.length - 1; y >= 0; y--) {
            System.out.print(arr[y] + " ");
        }
    }

    void arrAdd() {                 // Put an Array to Array
        int[] arrAuxiliaryAdd = {10, 20, 30};
        int numberOfElementsArrAdd = arrayLength + arrAuxiliaryAdd.length;
        int[] arrAuxiliarySum = new int[numberOfElementsArrAdd];
        for (int i = 0; i < arrayLength; i++) {
            arrAuxiliarySum[i] = arr[i];
        }
        for (int y = arrayLength, z = 0; y < arrAuxiliarySum.length; y++, z++) {
            arrAuxiliarySum[y] = arrAuxiliaryAdd[z];
        }
        System.out.println("\n" + "Sum of arrays is:");
        for (int w : arrAuxiliarySum) {
            System.out.print(w + " ");
        }
    }

    void arrDeleteDuplicate() {            // Delete duplicated elements
        int n = arrayLength;
        for (int i = 0, m = 0; i != n; i++, n = m) {
            for (int j = m = i + 1; j != n; j++) {
                if (arr[j] != arr[i]) {
                    if (m != j) arr[m] = arr[j];
                    m++;
                }
            }
        }
        if (n != arrayLength) {
            int[] b = new int[n];
            for (int i = 0; i < n; i++) b[i] = arr[i];

            arr = b;
        }
        System.out.println("\nYour Array without duplicated elements");
        for (int x : arr) System.out.print(x + " ");
        System.out.println();
    }

    void arrLineSearch(int valueOfElement) {                // Line search of 1st found value element
        arrAuxiliary = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            if (arr[i] == valueOfElement) {
                arrAuxiliary[counter] = i;
                counter++;
            }
        }
        int[] arrNumberOfElement = new int[counter];
        for (int w = 0; w < counter; w++) {
            arrNumberOfElement[w] = arrAuxiliary[w];
        }
        System.out.print("\nOur element is in position: ");
        for (int z : arrNumberOfElement) {
            System.out.print(z + " ");
        }
    }

    void arrShuffle() {                 // Shuffle Array in random order
        int[] arrRandom = new int[arrayLength];
        int[] arrRandomFin = new int[arrayLength];
        Random rand = new Random();
        for (int i = 0; i < arrayLength; i++) {
            arrRandom[i] = rand.nextInt(arrayLength);
            for (int y = 0; y < i; y++) {
                if ((arrRandom[i] == arrRandom[y]) && (i != y)) {
                    --i;
                    break;
                }
            }
        }
        for (int z = 0; z < arrayLength; z++) {
            arrRandomFin[arrRandom[z]] = arr[z];
        }
        System.out.println("\nArray was shuffle in random order and now it looks like:");
        for (int z : arrRandomFin) {
            System.out.print(z + " ");
        }
    }
}


