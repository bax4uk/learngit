package homeworks.imitation_list;

import java.util.Scanner;

public class MainImitationArrayList {
    public static void main(String[] args) {
        int counterMenu = 0;
        Scanner scanMenu = new Scanner(System.in);
        System.out.println("Welcome to Array Imitation Program! Choose what you want to do? But first please initiate your Array");
        ImitationArrayList obj = new ImitationArrayList();
        obj.initArray();
        System.out.print("\nThis is a menu. Enter a number of task and push 'Enter'"
                + "\nPress 1 to add element to your Array;"
                + "\nPress 2 to change element"
                + "\nPress 3 to remove element"
                + "\nPress 4 to change Array length"
                + "\nPress 5 to output your Array in direct and reverse way"
                + "\nPress 6 to add array to your Array"
                + "\nPress 7 to delete all duplicate in your Array"
                + "\nPress 8 to make line search of 1st first seen element"
                + "\nPress 9 to shuffle your Array in random way\n");


        switch (counterMenu = scanMenu.nextInt()) {
            case 1:
                obj.arrAddElement(66);
                break;
            case 2:
                obj.arrChangeElement(3, 777);
                break;
            case 3:
                obj.removeElementByIndex(0);
                break;
            case 4:
                obj.arrChangeLength(3);
                break;
            case 5:
                obj.arrOutput();
                break;
            case 6:
                obj.arrAdd();
                break;
            case 7:
                obj.arrDeleteDuplicate();
                break;
            case 8:
                obj.arrLineSearch(5);
                break;
            case 9:
                obj.arrShuffle();
                break;
            default:
                System.out.println("ERROR");
        }

    }
}
