package homeworks.two_dimension_array;

public class MainDiagonal {
    public static void main(String[] args) {
        int[][] mas = new int[3][3];
        int diag1 = 0;
        int diag2 = 0;
        int sumdiag = 0;
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                mas[i][j] = (int) (Math.random() * 10 + 1);
                System.out.println(mas[i][j]);
                if (i == j) {
                    diag1 += mas[i][j];
                }
                if (i + j == mas.length - 1) {
                    diag2 += mas[i][j];
                }
            }
        }
        sumdiag = diag1 + diag2;
        System.out.println("Summa diagonaley = " + sumdiag);

        for (int i = 0; i < mas.length; i++) {
           diag1 += mas[0][i];
           diag2 += mas[mas.length - 1][i];
        }
    }
}
