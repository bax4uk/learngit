alt + insert - create file, dir etc.
ctrl + d - duplication code
ctrl + y - remove line
ctrl + / - one line comment
ctrl + shift + / - many line comment
ctrl + shift + enter - create body + move to it
ctrl + shift + F10 - run psvm
ctrl + shift + space - autocomplete datatype
ctrl + alt + l - format code